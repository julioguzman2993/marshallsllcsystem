﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Collections.Generic;
using Aplicacion.Dto;
using Presentacion.MarshallsLLCWebApp.Models;
using Dominio.Model.Entities;
using Presentacion.MarshallsLLCWebApp.Controllers;
using System.Threading.Tasks;
using System.Web.Mvc;
using Infra.DataAcces.Repository;

namespace MarshallsLLCWebApp.UnitTest
{
    [TestClass]
    public class ServiceUnitTest
    {

        [TestMethod]
        public void TestEmployeeController()
        {
            var employeeController = new EmployeeController();
            var respuesta = employeeController.GetEmployeeAll();

            var employee = respuesta.ToString();

            Assert.IsNotNull(employee);


            var employeList = new EmployeeService().GetEmployees();

            Assert.IsNotNull(employeList);

        }

        [TestMethod]
        public void TestEmployeeRepositoryGetEmployeeAll()
        {
            var employeeRepository = new EmployeeRepository();
            
            var employeList = employeeRepository.GetEmployeeAll();

            List<Employee> viewModels = new List<Employee>();

            foreach (Employee item in employeList)
            {
                viewModels.Add(new Employee
                {
                    Id = item.Id,
                    Office = item.Office,
                    EmployeeCode = item.EmployeeCode,
                    EmployeeName = item.EmployeeName,
                    EmployeeSurname = item.EmployeeSurname,
                    Division = item.Division,
                    Position = item.Position,
                    Grade = item.Grade,
                    BeginDate = item.BeginDate,
                    Birthday = item.Birthday,
                    IdentificationNumber = item.IdentificationNumber,
                    BaseSalary = item.BaseSalary,
                    ProductionBonus = item.ProductionBonus,
                    CompensationBonus = item.CompensationBonus,
                    Commission = item.Commission,
                    Contributions = item.Contributions

                });
            }

            Assert.AreEqual(110, viewModels.Count);
        }

        [TestMethod]
        public void TestEmployeeRepositoryBuscEmpID()
        {
            var employeeRepository = new EmployeeRepository();

            var employeList = employeeRepository.BuscEmpID(1);

            List<Employee> viewModels = new List<Employee>();

            foreach (Employee item in employeList)
            {
                viewModels.Add(new Employee
                {
                    Id = item.Id,
                    Office = item.Office,
                    EmployeeCode = item.EmployeeCode,
                    EmployeeName = item.EmployeeName,
                    EmployeeSurname = item.EmployeeSurname,
                    Division = item.Division,
                    Position = item.Position,
                    Grade = item.Grade,
                    BeginDate = item.BeginDate,
                    Birthday = item.Birthday,
                    IdentificationNumber = item.IdentificationNumber,
                    BaseSalary = item.BaseSalary,
                    ProductionBonus = item.ProductionBonus,
                    CompensationBonus = item.CompensationBonus,
                    Commission = item.Commission,
                    Contributions = item.Contributions

                });
            }

            Assert.AreEqual(1, viewModels.Count);
        }

        [TestMethod]
        public void TestEmployeeRepositoryAddEmployee()
        {
            var employeeRepository = new EmployeeRepository();

            Employee employee = new Employee();

            employee.Office = "Office test";
            employee.EmployeeCode = "298";
            employee.EmployeeName = "Office test";
            employee.EmployeeSurname = "Office test";
            employee.Division = "Office test";
            employee.Position = "Office test";
            employee.Grade = 1;
            employee.BeginDate = Convert.ToDateTime("2020-02-02");
            employee.Birthday = Convert.ToDateTime("1991-02-02");
            employee.IdentificationNumber = "2761";
            employee.BaseSalary = 2000;
            employee.ProductionBonus = 0;
            employee.CompensationBonus = 0;
            employee.Commission = 0;
            employee.Contributions = 0;
            
            Assert.IsTrue(employeeRepository.AddEmployee(employee));

        }

        [TestMethod]
        public void TestEmployeeRepositoryEditEmployee()
        {
            var employeeRepository = new EmployeeRepository();

            Employee employee = new Employee();

            employee.Id = 126;
            employee.Office = "Office edit test";
            employee.EmployeeCode = "298";
            employee.EmployeeName = "Office edit test";
            employee.EmployeeSurname = "Office edit test";
            employee.Division = "Office edit test";
            employee.Position = "Office edit test";
            employee.Grade = 1;
            employee.BeginDate = Convert.ToDateTime("2020-02-02");
            employee.Birthday = Convert.ToDateTime("1991-02-02");
            employee.IdentificationNumber = "2761";
            employee.BaseSalary = 2000;
            employee.ProductionBonus = 0;
            employee.CompensationBonus = 0;
            employee.Commission = 0;
            employee.Contributions = 0;

            Assert.IsTrue(employeeRepository.EditEmployee(employee));

        }

        [TestMethod]
        public void TestEmployeeRepositoryDeleteEmployee()
        {
            var employeeRepository = new EmployeeRepository();

            Employee employee = new Employee();

            employee.Id = 126;

            Assert.IsTrue(employeeRepository.DeleteEmployee(employee));

        }

    }




}

﻿using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using Infra.DataAcces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Dto
{
    //Servicio para acceder a los repositorios de Divisions
    public class DivisionsService
    {
        readonly IDivisionsRepository divisionsRepository;


        public DivisionsService()
        {
            divisionsRepository = new DivisionsRepository();
        }

        public IEnumerable<Divisions> DivisionsList()
        {
            return divisionsRepository.DivisionsList();
        }
    }
}

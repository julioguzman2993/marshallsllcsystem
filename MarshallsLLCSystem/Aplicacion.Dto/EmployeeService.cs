﻿using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using Infra.DataAcces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Dto
{
    //Servicios para acceder a los repositorios de Empleados
    public class EmployeeService
    {
        readonly IEmployeeRepository employeeRepository;
        public EmployeeService()
        {
            employeeRepository = new EmployeeRepository();
        }
        public IEnumerable<Employee> GetEmployees()
        {
            return employeeRepository.GetEmployeeAll();
        }

        public bool AddEmployee(Employee employee)
        {

            return employeeRepository.AddEmployee(employee);

        }

        public bool EditEmployee(Employee employee)
        {

            return employeeRepository.EditEmployee(employee);

        }

        public bool DeleteEmployee(Employee employee)
        {

            return employeeRepository.DeleteEmployee(employee);

        }

        public string ValidarExistenciaEmployee(string IdentificationNumber)
        {

            return employeeRepository.ValidarExistenciaEmployee(IdentificationNumber);

        }

        public IEnumerable<Employee> GetEmployeeListByOfficeAndGrade(string Office, int Grade)
        {
            return employeeRepository.GetEmployeeListByOfficeAndGrade(Office, Grade);
        }

        public IEnumerable<Employee> GetEmployeeListByGrade(int Grade)
        {
            return employeeRepository.GetEmployeeListByGrade(Grade);
        }

        public IEnumerable<Employee> GetEmployeeListByPositionAndGrade(string Position, int Grade)
        {
            return employeeRepository.GetEmployeeListByPositionAndGrade(Position, Grade);
        }



    }
}

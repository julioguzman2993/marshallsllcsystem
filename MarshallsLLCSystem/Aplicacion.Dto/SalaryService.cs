﻿
using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using Infra.DataAcces.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Dto
{
    //Servicios para acceder a los repositorios de Salarios
    public class SalaryService
    {
        readonly ISalaryRepository salaryRepository;

        readonly IEmployeeRepository employeeRepository;

        public SalaryService()
        {
            salaryRepository = new SalaryRepository();
            employeeRepository = new EmployeeRepository();
        }
        public bool AddSalary(Salary salary)
        {

            return salaryRepository.AddSalary(salary);

        }

        public IEnumerable<Employee> BuscEmpID(int id)
        {
            return employeeRepository.BuscEmpID(id);

        }

        public string ValidarPagoMes(List<Salary> ListaPagoSalario)
        {

            return salaryRepository.ValidarPagoMes(ListaPagoSalario);

        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryYearAndMonth(int PayYear, int PayMonth)
        {
            return salaryRepository.GetSalaryYearAndMonth(PayYear, PayMonth);
        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryByIdEmployee(int IdEmployee)
        {
            return salaryRepository.GetSalaryByIdEmployee(IdEmployee);
        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryByEmployeeName(string EmployeeName)
        {
            return salaryRepository.GetSalaryByEmployeeName(EmployeeName);
        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryByEmployeeSurname(string EmployeeSurname)
        {
            return salaryRepository.GetSalaryByEmployeeSurname(EmployeeSurname);
        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryByIdentificationNumber(string IdentificationNumber)
        {
            return salaryRepository.GetSalaryByIdentificationNumber(IdentificationNumber);
        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryByTresMesesConsecutivos(int IdEmployee)
        {
            return salaryRepository.GetSalaryByTresMesesConsecutivos(IdEmployee);
        }

        public IEnumerable<ConsultaSalaryModel> GetSalaryByDosMesesConsecutivos(int IdEmployee)
        {
            return salaryRepository.GetSalaryByDosMesesConsecutivos(IdEmployee);
        }


        
    }
}


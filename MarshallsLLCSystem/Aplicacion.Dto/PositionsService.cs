﻿using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using Infra.DataAcces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Dto
{
    //Servicio para acceder a los repositorios de Positions
    public class PositionsService
    {
        readonly IPositionsRepository positionsRepository;


        public PositionsService()
        {
            positionsRepository = new PositionsRepository();
        }

        public IEnumerable<Positions> PositionsList()
        {
            return positionsRepository.PositionsList();
        }
    }
}

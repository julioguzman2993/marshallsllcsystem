﻿using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using Infra.DataAcces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Dto
{
    //Servicio para acceder a los repositorios de Oficces
    public class OfficeService
    {
        readonly IOficcesRepository oficcesRepository;
        

        public OfficeService()
        {
            oficcesRepository = new OficcesRepository();
        }

        public IEnumerable<Offices> OfficesList()
        {
            return oficcesRepository.OfficesList();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Presentacion.MarshallsLLCWebApp.Models;
using Aplicacion.Dto;
using Dominio.Model.Entities;
using Dominio.Model.ObjectValues;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace Presentacion.MarshallsLLCWebApp.Controllers
{
    public class SalaryController : Controller
    {
        private SalaryService salaryService = new SalaryService();
        private OfficeService officeService = new OfficeService();
        private DivisionsService divisionsService = new DivisionsService();
        private PositionsService positionsService = new PositionsService();

        private Salary salary = new Salary();
        private CalculoSalario calculoSalario = new CalculoSalario();
        private GetActualDate getActualDate = new GetActualDate();
        private ConsultaSalaryModel consultaSalaryModel = new ConsultaSalaryModel();
        private ItemsBuscarPor itemsBuscarPor = new ItemsBuscarPor();

        //Codigos para mostrar la lista de los empleados a los que se les va a procesar los pagos salariales
        public ActionResult GetEmployeeAllForPay(string txtBuscarPor = "")
        {
            
            var employeList = new EmployeeService().GetEmployees();
            List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();
            if (txtBuscarPor == "")
            {

                foreach (Employee item in employeList)
                {
                    viewModels.Add(new EmployeeViewModel
                    {
                        Id = item.Id,
                        Office = item.Office,
                        EmployeeCode = item.EmployeeCode,
                        EmployeeName = item.EmployeeName,
                        EmployeeSurname = item.EmployeeSurname,
                        Division = item.Division,
                        Position = item.Position,
                        Grade = item.Grade,
                        BeginDate = item.BeginDate,
                        Birthday = item.Birthday,
                        IdentificationNumber = item.IdentificationNumber,
                        BaseSalary = item.BaseSalary,
                        ProductionBonus = item.ProductionBonus,
                        CompensationBonus = item.CompensationBonus,
                        Commission = item.Commission,
                        Contributions = item.Contributions

                    });
                }
                return View(viewModels);
            }
            
            if (txtBuscarPor != "")
            {
                if (Regex.IsMatch(txtBuscarPor, @"^[0-9]+$"))
                {
                    var employeListId = new SalaryService().BuscEmpID(Convert.ToInt32(txtBuscarPor));
                    
                    List<EmployeeViewModel> employeeViewModel = new List<EmployeeViewModel>();

                    foreach (Employee item in employeListId)
                    {
                        employeeViewModel.Add(new EmployeeViewModel
                        {
                            Id = item.Id,
                            Office = item.Office,
                            EmployeeCode = item.EmployeeCode,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            Grade = item.Grade,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday,
                            IdentificationNumber = item.IdentificationNumber,
                            BaseSalary = item.BaseSalary,
                            ProductionBonus = item.ProductionBonus,
                            CompensationBonus = item.CompensationBonus,
                            Commission = item.Commission,
                            Contributions = item.Contributions

                        });
                    }

                    return View(employeeViewModel);
                }
                else
                {
                    List<EmployeeViewModel> viewModelsList = new List<EmployeeViewModel>();
                    return View(viewModelsList);
                }

            }
            try
            {
                


            }
            catch (Exception)
            {
                return View();
            }
            return View(viewModels);
        }

        //Codigos para cargar los datos del empleado en la vista
        [HttpGet]
        public ActionResult AddSalary(int id)
        {
            
            try
            {
                var employeList = new SalaryService().BuscEmpID(id);

                SalaryViewModel salaryViewModel = new SalaryViewModel();

                foreach (Employee item in employeList)
                {
                    salaryViewModel.IdEmployee = item.Id;
                    salaryViewModel.IdentificationNumber = item.IdentificationNumber;
                    salaryViewModel.BaseSalary = item.BaseSalary;
                    salaryViewModel.ProductionBonus = item.ProductionBonus;
                    salaryViewModel.CompensationBonus = item.CompensationBonus;
                    salaryViewModel.Commission = item.Commission;
                    salaryViewModel.Contributions = item.Contributions;
                    salaryViewModel.TotalSalary = calculoSalario.CalcluloTotalSalary(item.BaseSalary, item.ProductionBonus, item.CompensationBonus, item.Commission, item.Contributions);
                }

                return View(salaryViewModel);
            }
            catch (Exception)
            {
                return View();
            }
            
        }
        
        //Codigos para registrar el salario al empleado selecionado
        [HttpPost]
        public ActionResult AddSalary(List<SalaryViewModel> ListaPagoSalario)
        {
            try
            {
                string mensaje = "";
                string validacion = "";
                //Validacion para cofirmar los meses pagos de los empleados
                List<Salary> ListSalary = new List<Salary>();
                foreach (var data in ListaPagoSalario)
                {
                    salary.IdEmployee = Convert.ToInt32(data.IdEmployee);
                    salary.PayYear = Convert.ToInt32(data.PayYear);
                    salary.PayMonth = Convert.ToInt32(data.PayMonth);

                    ListSalary.Add(salary);
                    validacion = salaryService.ValidarPagoMes(ListSalary);
                    if (validacion != "Ninguno")
                    {
                        return Json(validacion);
                    }
                }
                
                //Codigos para registrar los pagos a los empleados
                if (validacion == "Ninguno")
                {
                    foreach (var data in ListaPagoSalario)
                    {
                        salary.IdEmployee = Convert.ToInt32(data.IdEmployee);
                        salary.IdentificationNumber = data.IdentificationNumber;
                        salary.BaseSalary = Convert.ToDecimal(data.BaseSalary);
                        salary.ProductionBonus = Convert.ToDecimal(data.ProductionBonus);
                        salary.CompensationBonus = Convert.ToDecimal(data.CompensationBonus);
                        salary.Commission = Convert.ToDecimal(data.Commission);
                        salary.Contributions = Convert.ToDecimal(data.Contributions);
                        salary.TotalSalary = Convert.ToDecimal(data.TotalSalary);
                        salary.PayYear = Convert.ToInt32(data.PayYear);
                        salary.PayMonth = Convert.ToInt32(data.PayMonth);
                        salary.PayDay = getActualDate.GetActualDay();

                        salaryService.AddSalary(salary);
                        mensaje = "Proceso realizado";


                    }

                }
                else
                {
                    return Json(validacion);
                }


                return Json(mensaje);

            }
            catch (Exception ex)
            {
                return Json("Error! " + ex.Message);
            }
            
            
        }

        //Cargar Lista de salarios pagos
        public ActionResult GetSalaryList(string BuscarPorList = "", string txtBuscarPor = "",string DDLYear = "",string DDLMonth = "")
        {
            
            ViewBag.OfficesList = new SelectList(officeService.OfficesList(), "NameOffices", "NameOffices");
            ViewBag.PosicionList = new SelectList(divisionsService.DivisionsList(), "NameDivisions", "NameDivisions");
            ViewBag.DivisionList = new SelectList(positionsService.PositionsList(), "NamePositions", "NamePositions");
            ViewBag.BuscarPorList = new SelectList(itemsBuscarPor.ListaItems(), "BuscarPor", "BuscarPor");
            ViewBag.DDLYear = new SelectList(getActualDate.GetListYear(), "ListYears", "ListYears");
            ViewBag.DDLMonth = new SelectList(getActualDate.GetListMonth(), "ListMonths", "ListMonths");
            if (DDLYear == "")
            {
                DDLYear = getActualDate.GetActualYear().ToString();
            }
            if (DDLMonth == "")
            {
                DDLMonth = getActualDate.GetActualMonth().ToString();
            }

            if (BuscarPorList == "" || BuscarPorList == "Todos" && txtBuscarPor == "")
            {
                var salaryList = new SalaryService().GetSalaryYearAndMonth(Convert.ToInt32(DDLYear), Convert.ToInt32(DDLMonth));
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                foreach (ConsultaSalaryModel item in salaryList)
                {
                    viewModels.Add(new ConsultaSalaryViewModel
                    {
                        Id = item.Id,
                        IdEmployee = item.IdEmployee,
                        IdentificationNumber = item.IdentificationNumber,
                        BaseSalary = item.BaseSalary,
                        ProductionBonus = item.ProductionBonus,
                        CompensationBonus = item.CompensationBonus,
                        Commission = item.Commission,
                        Contributions = item.Contributions,
                        TotalSalary = item.TotalSalary,
                        PayYear = item.PayYear,
                        PayMonth = item.PayMonth,
                        PayDay = item.PayDay,
                        Office = item.Office,
                        EmployeeName = item.EmployeeName,
                        EmployeeSurname = item.EmployeeSurname,
                        Division = item.Division,
                        Position = item.Position,
                        Grade = item.Grade,
                        BeginDate = item.BeginDate,
                        Birthday = item.Birthday

                    });
                }

                return View(viewModels);

            }
            if (BuscarPorList == "Todos" && txtBuscarPor != "")
            {
                var salaryList = new SalaryService().GetSalaryByEmployeeName(txtBuscarPor);
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                foreach (ConsultaSalaryModel item in salaryList)
                {
                    viewModels.Add(new ConsultaSalaryViewModel
                    {
                        Id = item.Id,
                        IdEmployee = item.IdEmployee,
                        IdentificationNumber = item.IdentificationNumber,
                        BaseSalary = item.BaseSalary,
                        ProductionBonus = item.ProductionBonus,
                        CompensationBonus = item.CompensationBonus,
                        Commission = item.Commission,
                        Contributions = item.Contributions,
                        TotalSalary = item.TotalSalary,
                        PayYear = item.PayYear,
                        PayMonth = item.PayMonth,
                        PayDay = item.PayDay,
                        Office = item.Office,
                        EmployeeName = item.EmployeeName,
                        EmployeeSurname = item.EmployeeSurname,
                        Division = item.Division,
                        Position = item.Position,
                        Grade = item.Grade,
                        BeginDate = item.BeginDate,
                        Birthday = item.Birthday

                    });
                }

                return View(viewModels);

            }
            else if (txtBuscarPor != "")
            {
                if (BuscarPorList == "IdEmployee")
                {
                    //Validar si se introduce un numero para buscar por Id
                    if (Regex.IsMatch(txtBuscarPor, @"^[0-9]+$"))
                    {
                        var salaryList = new SalaryService().GetSalaryByIdEmployee(Convert.ToInt32(txtBuscarPor));
                        List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                        foreach (ConsultaSalaryModel item in salaryList)
                        {
                            viewModels.Add(new ConsultaSalaryViewModel
                            {
                                Id = item.Id,
                                IdEmployee = item.IdEmployee,
                                IdentificationNumber = item.IdentificationNumber,
                                BaseSalary = item.BaseSalary,
                                ProductionBonus = item.ProductionBonus,
                                CompensationBonus = item.CompensationBonus,
                                Commission = item.Commission,
                                Contributions = item.Contributions,
                                TotalSalary = item.TotalSalary,
                                PayYear = item.PayYear,
                                PayMonth = item.PayMonth,
                                PayDay = item.PayDay,
                                Office = item.Office,
                                EmployeeName = item.EmployeeName,
                                EmployeeSurname = item.EmployeeSurname,
                                Division = item.Division,
                                Position = item.Position,
                                Grade = item.Grade,
                                BeginDate = item.BeginDate,
                                Birthday = item.Birthday

                            });
                        }

                        return View(viewModels);
                    }
                    else
                    {
                        List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();
                        return View(viewModels);
                    }
                    

                    
                    

                }
                if (BuscarPorList == "EmployeeName")
                {
                    var salaryList = new SalaryService().GetSalaryByEmployeeName(txtBuscarPor);
                    List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                    foreach (ConsultaSalaryModel item in salaryList)
                    {
                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            Id = item.Id,
                            IdEmployee = item.IdEmployee,
                            IdentificationNumber = item.IdentificationNumber,
                            BaseSalary = item.BaseSalary,
                            ProductionBonus = item.ProductionBonus,
                            CompensationBonus = item.CompensationBonus,
                            Commission = item.Commission,
                            Contributions = item.Contributions,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            PayMonth = item.PayMonth,
                            PayDay = item.PayDay,
                            Office = item.Office,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            Grade = item.Grade,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday

                        });
                    }

                    return View(viewModels);
                }
                if (BuscarPorList == "EmployeeSurname")
                {
                    var salaryList = new SalaryService().GetSalaryByEmployeeSurname(txtBuscarPor);
                    List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                    foreach (ConsultaSalaryModel item in salaryList)
                    {
                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            Id = item.Id,
                            IdEmployee = item.IdEmployee,
                            IdentificationNumber = item.IdentificationNumber,
                            BaseSalary = item.BaseSalary,
                            ProductionBonus = item.ProductionBonus,
                            CompensationBonus = item.CompensationBonus,
                            Commission = item.Commission,
                            Contributions = item.Contributions,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            PayMonth = item.PayMonth,
                            PayDay = item.PayDay,
                            Office = item.Office,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            Grade = item.Grade,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday

                        });
                    }

                    return View(viewModels);
                }
                if (BuscarPorList == "IdentificationNumber")
                {
                    var salaryList = new SalaryService().GetSalaryByIdentificationNumber(txtBuscarPor);
                    List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                    foreach (ConsultaSalaryModel item in salaryList)
                    {
                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            Id = item.Id,
                            IdEmployee = item.IdEmployee,
                            IdentificationNumber = item.IdentificationNumber,
                            BaseSalary = item.BaseSalary,
                            ProductionBonus = item.ProductionBonus,
                            CompensationBonus = item.CompensationBonus,
                            Commission = item.Commission,
                            Contributions = item.Contributions,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            PayMonth = item.PayMonth,
                            PayDay = item.PayDay,
                            Office = item.Office,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            Grade = item.Grade,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday

                        });
                    }

                    return View(viewModels);
                }
            }
            List<ConsultaSalaryViewModel> viewModelsEmty = new List<ConsultaSalaryViewModel>();
            return View(viewModelsEmty);
        }

        //Codigos para buscar empleados mediantes filtros
        public ActionResult GetAdvancedSalaryList(string txtBuscId = "", string DDBuscarPorMeses = "")
        {
            
            int id = 0;
            string ParametroBusqueda = "";
            int countPayMonth = 0;
            ViewBag.DDBuscarPorMeses = new SelectList(itemsBuscarPor.ListaItemsBuscSalario(), "BuscarPor", "BuscarPor");
            //Validar si se introduce un numero para buscar por Id
            if (Regex.IsMatch(txtBuscId, @"^[0-9]+$"))
            {
                if (txtBuscId != "")
                {
                    id = Convert.ToInt32(txtBuscId);
                }
                if (DDBuscarPorMeses != "")
                {
                    ParametroBusqueda = DDBuscarPorMeses;
                }

                
            }
            else
            {
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                return View(viewModels);
            }

            
            var employeList = new SalaryService().BuscEmpID(id);

            EmployeeViewModel employeeViewModel = new EmployeeViewModel();

            foreach (Employee item in employeList)
            {
                employeeViewModel.EmployeeName = item.EmployeeName;
                employeeViewModel.EmployeeSurname = item.EmployeeSurname;
                employeeViewModel.IdentificationNumber = item.IdentificationNumber;
                employeeViewModel.Office = item.Office;
                employeeViewModel.Position = item.Position;
                employeeViewModel.Grade = item.Grade;



            }

            ViewBag.Nombre = employeeViewModel.EmployeeName + " " + employeeViewModel.EmployeeSurname;
            ViewBag.NumeroIdentificacion = employeeViewModel.IdentificationNumber;
            ViewBag.Office = employeeViewModel.Office;
            ViewBag.Position = employeeViewModel.Position;
            ViewBag.Grade = employeeViewModel.Grade;

            if (employeeViewModel.Office == null)
            {
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                return View(viewModels);
            }

            if (id == 0)
            {
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();
                return View(viewModels);

            }
            
            if (ParametroBusqueda == "")
            {
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();
                return View(viewModels);
            }
                if (ParametroBusqueda == "3 meses consecutivos")
            {
                

                var salaryList = new SalaryService().GetSalaryByTresMesesConsecutivos(id);
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                foreach (ConsultaSalaryModel item in salaryList)
                {
                    if (countPayMonth == 0)
                    {
                        countPayMonth = item.PayMonth;

                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            PayMonth = item.PayMonth,
                            Id = item.Id,
                            IdentificationNumber = item.IdentificationNumber,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            Bono = item.TotalSalary / 3


                        });

                        countPayMonth = countPayMonth - 1;


                    }

                    if (item.PayMonth == countPayMonth)
                    {
                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            PayMonth = item.PayMonth,
                            Id = item.Id,
                            IdentificationNumber = item.IdentificationNumber,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            Bono = item.TotalSalary / 3


                        });
                        countPayMonth = countPayMonth - 1;
                    }

                    if (item.PayMonth == countPayMonth)
                    {
                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            PayMonth = item.PayMonth,
                            Id = item.Id,
                            IdentificationNumber = item.IdentificationNumber,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            Bono = item.TotalSalary / 3


                        });
                        break;
                    }

                    
                    
                    

                }


                return View(viewModels);

            }

            if (ParametroBusqueda == "2 meses consecutivos")
            {


                var salaryList = new SalaryService().GetSalaryByDosMesesConsecutivos(id);
                List<ConsultaSalaryViewModel> viewModels = new List<ConsultaSalaryViewModel>();

                foreach (ConsultaSalaryModel item in salaryList)
                {
                    if (countPayMonth == 0)
                    {
                        countPayMonth = item.PayMonth;

                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            PayMonth = item.PayMonth,
                            Id = item.Id,
                            IdentificationNumber = item.IdentificationNumber,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            Bono = item.TotalSalary / 3


                        });

                        countPayMonth = countPayMonth - 1;


                    }

                    if (item.PayMonth == countPayMonth)
                    {
                        viewModels.Add(new ConsultaSalaryViewModel
                        {
                            PayMonth = item.PayMonth,
                            Id = item.Id,
                            IdentificationNumber = item.IdentificationNumber,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            TotalSalary = item.TotalSalary,
                            PayYear = item.PayYear,
                            Bono = item.TotalSalary / 3


                        });
                    }
                    

                }


                return View(viewModels);

            }

            return View();
        }

    }
}
﻿using Aplicacion.Dto;
using Dominio.Model.Entities;
using Dominio.Model.ObjectValues;
using Dominio.Model.Validaciones;
using Presentacion.MarshallsLLCWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentacion.MarshallsLLCWebApp.Controllers
{
    public class EmployeeController : Controller
    {
        //A continuacion se muestran las referencias a las clases a la ue accede este controlador
        private Employee employee = new Employee();
        private EmployeeService employeeService = new EmployeeService();
        private ItemsBuscarPor itemsBuscarPor = new ItemsBuscarPor();
        private CalculoSalario calculoSalario = new CalculoSalario();
        private OfficeService officeService = new OfficeService();
        private DivisionsService divisionsService = new DivisionsService();
        private PositionsService positionsService = new PositionsService();
        private ValidarCamposEmployee validarCamposEmployee = new ValidarCamposEmployee();

        //Codigos para cargar los datos del empleado en la vista 
        public ActionResult GetEmployeeAll()
        {
            
            
            try
            {
                var employeList = new EmployeeService().GetEmployees();
                List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                foreach (Employee item in employeList)
                {
                    viewModels.Add(new EmployeeViewModel
                    {
                        Id = item.Id,
                        Office = item.Office,
                        EmployeeCode = item.EmployeeCode,
                        EmployeeName = item.EmployeeName,
                        EmployeeSurname = item.EmployeeSurname,
                        Division = item.Division,
                        Position = item.Position,
                        Grade = item.Grade,
                        BeginDate = item.BeginDate,
                        Birthday = item.Birthday,
                        IdentificationNumber = item.IdentificationNumber,
                        BaseSalary = item.BaseSalary,
                        ProductionBonus = item.ProductionBonus,
                        CompensationBonus = item.CompensationBonus,
                        Commission = item.Commission,
                        Contributions = item.Contributions

                    });
                }
                return View(viewModels);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error - " + ex.Message;
                return View();
            }
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            //Codigos para cargar la vista de registro de empleados
            try
            {
                CargarDropDownList();
                return View();
            }
            catch (Exception)
            {
                return View();
            }
            

        }

        //Codigos Para Insertar empleados
        [HttpPost]
        public ActionResult AddEmployee(EmployeeViewModel employeeViewModel)
        {
            
            string validacion = "";
            
            try
            {
           
                validacion = employeeService.ValidarExistenciaEmployee(employeeViewModel.IdentificationNumber);

                if (validacion == "Ninguno")
                {
                    employee.Id = employeeViewModel.Id;
                    employee.Office = employeeViewModel.Office;
                    employee.EmployeeCode = employeeViewModel.EmployeeCode;
                    employee.EmployeeName = employeeViewModel.EmployeeName;
                    employee.EmployeeSurname = employeeViewModel.EmployeeSurname;
                    employee.Division = employeeViewModel.Division;
                    employee.Position = employeeViewModel.Position;
                    employee.Grade = employeeViewModel.Grade;
                    employee.BeginDate = employeeViewModel.BeginDate;
                    employee.Birthday = employeeViewModel.Birthday;
                    employee.IdentificationNumber = employeeViewModel.IdentificationNumber;
                    employee.BaseSalary = employeeViewModel.BaseSalary;
                    employee.ProductionBonus = employeeViewModel.ProductionBonus;
                    employee.CompensationBonus = employeeViewModel.CompensationBonus;
                    employee.Commission = employeeViewModel.Commission;
                    employee.Contributions = employeeViewModel.Contributions;

                    if (validarCamposEmployee.ValidarDatosEmployee(employee))
                    {
                        employeeService.AddEmployee(employee);
                        //return RedirectToAction("GetEmployeeAll");
                        CargarDropDownList();
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorRegisto = "Favor de llenar todos los compos";
                        CargarDropDownList();
                        return View();
                    }

                    
                }
                else
                {
                    ViewBag.ErrorRegisto = validacion;
                    CargarDropDownList();
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorRegisto = "Error - " + ex.Message;
                CargarDropDownList();
                return View();
            }
            

        }

        //Codigos para cargar los datos del empleado en la vista
        [HttpGet]
        public ActionResult EditEmployee(int id)
        {
            
            try
            {
                CargarDropDownList();

                var employeList = new SalaryService().BuscEmpID(id);

                EmployeeViewModel employeeViewModel = new EmployeeViewModel();

                foreach (Employee item in employeList)
                {
                    employeeViewModel.Office = item.Office;
                    employeeViewModel.EmployeeCode = item.EmployeeCode;
                    employeeViewModel.EmployeeName = item.EmployeeName;
                    employeeViewModel.EmployeeSurname = item.EmployeeSurname;
                    employeeViewModel.Division = item.Division;
                    employeeViewModel.Position = item.Position;
                    employeeViewModel.Grade = item.Grade;
                    employeeViewModel.BeginDate = item.BeginDate;
                    employeeViewModel.Birthday = item.Birthday;
                    employeeViewModel.IdentificationNumber = item.IdentificationNumber;
                    employeeViewModel.BaseSalary = item.BaseSalary;
                    employeeViewModel.ProductionBonus = item.ProductionBonus;
                    employeeViewModel.CompensationBonus = item.CompensationBonus;
                    employeeViewModel.Commission = item.Commission;
                    employeeViewModel.Contributions = item.Contributions;
                }

                return View(employeeViewModel);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error - " + ex.Message;
                return View();
            }

        }

        //Codigos para cargar modificar datos del empleado
        [HttpPost]
        public ActionResult EditEmployee(EmployeeViewModel employeeViewModel)
        {
            
            try
            {
                employee.Id = employeeViewModel.Id;
                employee.Office = employeeViewModel.Office;
                employee.EmployeeCode = employeeViewModel.EmployeeCode;
                employee.EmployeeName = employeeViewModel.EmployeeName;
                employee.EmployeeSurname = employeeViewModel.EmployeeSurname;
                employee.Division = employeeViewModel.Division;
                employee.Position = employeeViewModel.Position;
                employee.Grade = employeeViewModel.Grade;
                employee.BeginDate = employeeViewModel.BeginDate;
                employee.Birthday = employeeViewModel.Birthday;
                employee.IdentificationNumber = employeeViewModel.IdentificationNumber;
                employee.BaseSalary = employeeViewModel.BaseSalary;
                employee.ProductionBonus = employeeViewModel.ProductionBonus;
                employee.CompensationBonus = employeeViewModel.CompensationBonus;
                employee.Commission = employeeViewModel.Commission;
                employee.Contributions = employeeViewModel.Contributions;

                employeeService.EditEmployee(employee);
                CargarDropDownList();
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error - " + ex.Message;
                return View();
            }

        }

        //Codigos para cargar los datos del empleado en la vista
        [HttpGet]
        public ActionResult DeleteEmployee(int id)
        {
            
            try
            {
                var employeList = new SalaryService().BuscEmpID(id);

                EmployeeViewModel employeeViewModel = new EmployeeViewModel();

                foreach (Employee item in employeList)
                {
                    employeeViewModel.Office = item.Office;
                    employeeViewModel.EmployeeCode = item.EmployeeCode;
                    employeeViewModel.EmployeeName = item.EmployeeName;
                    employeeViewModel.EmployeeSurname = item.EmployeeSurname;
                    employeeViewModel.Division = item.Division;
                    employeeViewModel.Position = item.Position;
                    employeeViewModel.Grade = item.Grade;
                    employeeViewModel.BeginDate = item.BeginDate;
                    employeeViewModel.Birthday = item.Birthday;
                    employeeViewModel.IdentificationNumber = item.IdentificationNumber;
                    employeeViewModel.BaseSalary = item.BaseSalary;
                    employeeViewModel.ProductionBonus = item.ProductionBonus;
                    employeeViewModel.CompensationBonus = item.CompensationBonus;
                    employeeViewModel.Commission = item.Commission;
                    employeeViewModel.Contributions = item.Contributions;
                }

                return View(employeeViewModel);
            }
            catch (Exception)
            {
                return View();
            }

        }

        //Codigos para eliminar un empleado
        [HttpPost]
        public ActionResult DeleteEmployee(EmployeeViewModel employeeViewModel)
        {
            
            try
            {
                employee.Id = employeeViewModel.Id;

                employeeService.DeleteEmployee(employee);

                return RedirectToAction("GetEmployeeAll");
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error - " + ex.Message;
                return View();
            }
        }

        //Codigos para buscar empleados mediantes filtros
        public ActionResult GetAdvancedEmployeeList(string txtBuscarPor = "", string DDBuscarPorArea = "")
        {
            
            try
            {
                int id = 0;
                string ParametroBusqueda = "";
                if (txtBuscarPor != "")
                {
                    id = Convert.ToInt32(txtBuscarPor);
                }
                if (DDBuscarPorArea != "")
                {
                    ParametroBusqueda = DDBuscarPorArea;
                }

                ViewBag.DDBuscarPorArea = new SelectList(itemsBuscarPor.ListaItemsBuscEmpleado(), "BuscarPor", "BuscarPor");
                var employeList = new SalaryService().BuscEmpID(id);

                EmployeeViewModel employeeViewModel = new EmployeeViewModel();

                foreach (Employee item in employeList)
                {
                    employeeViewModel.EmployeeName = item.EmployeeName;
                    employeeViewModel.EmployeeSurname = item.EmployeeSurname;
                    employeeViewModel.IdentificationNumber = item.IdentificationNumber;
                    employeeViewModel.Office = item.Office;
                    employeeViewModel.Position = item.Position;
                    employeeViewModel.Grade = item.Grade;



                }

                ViewBag.Nombre = employeeViewModel.EmployeeName + " " + employeeViewModel.EmployeeSurname;
                ViewBag.NumeroIdentificacion = employeeViewModel.IdentificationNumber;
                ViewBag.Office = employeeViewModel.Office;
                ViewBag.Position = employeeViewModel.Position;
                ViewBag.Grade = employeeViewModel.Grade;

                if (employeeViewModel.Office == null)
                {
                    List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                    return View(viewModels);
                }

                if (id == 0)
                {
                    List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();
                    return View(viewModels);

                }

                if (employeeViewModel.Office != null)
                {
                    if (ParametroBusqueda == "")
                    {
                        List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                        return View(viewModels);
                    }

                }

                if (ParametroBusqueda == "Misma Oficina y Grado")
                {
                    var employeeList = new EmployeeService().GetEmployeeListByOfficeAndGrade(employeeViewModel.Office, employeeViewModel.Grade);
                    List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                    foreach (Employee item in employeeList)
                    {
                        viewModels.Add(new EmployeeViewModel
                        {
                            Id = item.Id,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday,
                            IdentificationNumber = item.IdentificationNumber,
                            TotalSalary = calculoSalario.CalcluloTotalSalary(item.BaseSalary, item.ProductionBonus, item.CompensationBonus, item.Commission, item.Contributions)



                        });
                    }


                    return View(viewModels);


                }

                else if (ParametroBusqueda == "Todas Oficinas y Mismo Grado")
                {
                    var employeeList = new EmployeeService().GetEmployeeListByGrade(employeeViewModel.Grade);
                    List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                    foreach (Employee item in employeeList)
                    {
                        viewModels.Add(new EmployeeViewModel
                        {
                            Id = item.Id,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday,
                            IdentificationNumber = item.IdentificationNumber,
                            TotalSalary = calculoSalario.CalcluloTotalSalary(item.BaseSalary, item.ProductionBonus, item.CompensationBonus, item.Commission, item.Contributions)



                        });
                    }


                    return View(viewModels);


                }

                else if (ParametroBusqueda == "Misma Pocision y Grado")
                {
                    var employeeList = new EmployeeService().GetEmployeeListByPositionAndGrade(employeeViewModel.Position, employeeViewModel.Grade);
                    List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                    foreach (Employee item in employeeList)
                    {
                        viewModels.Add(new EmployeeViewModel
                        {
                            Id = item.Id,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday,
                            IdentificationNumber = item.IdentificationNumber,
                            TotalSalary = calculoSalario.CalcluloTotalSalary(item.BaseSalary, item.ProductionBonus, item.CompensationBonus, item.Commission, item.Contributions)



                        });
                    }


                    return View(viewModels);


                }

                else if (ParametroBusqueda == "Tadas Pocisiones y Mismo Grado")
                {
                    var employeeList = new EmployeeService().GetEmployeeListByGrade(employeeViewModel.Grade);
                    List<EmployeeViewModel> viewModels = new List<EmployeeViewModel>();

                    foreach (Employee item in employeeList)
                    {
                        viewModels.Add(new EmployeeViewModel
                        {
                            Id = item.Id,
                            EmployeeName = item.EmployeeName,
                            EmployeeSurname = item.EmployeeSurname,
                            Division = item.Division,
                            Position = item.Position,
                            BeginDate = item.BeginDate,
                            Birthday = item.Birthday,
                            IdentificationNumber = item.IdentificationNumber,
                            TotalSalary = calculoSalario.CalcluloTotalSalary(item.BaseSalary, item.ProductionBonus, item.CompensationBonus, item.Commission, item.Contributions)



                        });
                    }


                    return View(viewModels);


                }

            }
            catch (Exception)
            {
                return View();
            }

            return View();


        }

        //codigos para cargar los DropDownListFor Offices, Divisions, Positions de las vistas
        public void CargarDropDownList()
        {
            ViewBag.OfficesList = new SelectList(officeService.OfficesList(), "NameOffices", "NameOffices");
            ViewBag.DivisionList = new SelectList(divisionsService.DivisionsList(), "NameDivisions", "NameDivisions");
            ViewBag.PosicionList = new SelectList(positionsService.PositionsList(), "NamePositions", "NamePositions");
        }


    }
}

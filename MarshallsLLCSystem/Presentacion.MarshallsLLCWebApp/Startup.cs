﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Presentacion.MarshallsLLCWebApp.Startup))]
namespace Presentacion.MarshallsLLCWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

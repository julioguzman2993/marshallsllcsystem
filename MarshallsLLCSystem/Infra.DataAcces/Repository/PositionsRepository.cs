﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using System.Data.SqlClient;

namespace Infra.DataAcces.Repository
{
    //Codigos para extraer la lista de Positions de la base de datos
    public class PositionsRepository : ConnectionSQL, IPositionsRepository
    {
        public IEnumerable<Positions> PositionsList()
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select * from Positions";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Positions> ListaGenerica = new List<Positions>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Positions
                {
                    Id = LeerFilas.GetInt32(0),
                    NamePositions = LeerFilas.GetString(1),
                    InformationPositions = LeerFilas.GetString(2)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }
    }
}

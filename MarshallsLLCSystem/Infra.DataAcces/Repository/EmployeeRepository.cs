﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using System.Data.SqlClient;
using System.Data;

namespace Infra.DataAcces.Repository
{
    public class EmployeeRepository : ConnectionSQL, IEmployeeRepository
    {
        //Codigos para optener la lista de todos los empleados
        public IEnumerable<Employee> GetEmployeeAll()
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select * from Employees order by EmployeeName";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Employee> ListaGenerica = new List<Employee>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Employee
                {
                    Id = LeerFilas.GetInt32(0),
                    Office = LeerFilas.GetString(1),
                    EmployeeCode = LeerFilas.GetString(2),
                    EmployeeName = LeerFilas.GetString(3),
                    EmployeeSurname = LeerFilas.GetString(4),
                    Division = LeerFilas.GetString(5),
                    Position = LeerFilas.GetString(6),
                    Grade = LeerFilas.GetInt32(7),
                    BeginDate = LeerFilas.GetDateTime(8),
                    Birthday = LeerFilas.GetDateTime(9),
                    IdentificationNumber = LeerFilas.GetString(10),
                    BaseSalary = LeerFilas.GetDecimal(11),
                    ProductionBonus = LeerFilas.GetDecimal(12),
                    CompensationBonus = LeerFilas.GetDecimal(13),
                    Commission = LeerFilas.GetDecimal(14),
                    Contributions = LeerFilas.GetDecimal(15)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }
        
        //Codigo para buscar empleado por id
        public IEnumerable<Employee> BuscEmpID(int id)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select * from Employees where Id = @Id";
            cmd.Parameters.AddWithValue("@Id", id);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Employee> ListaGenerica = new List<Employee>();
            if (LeerFilas.Read())
            {
                ListaGenerica.Add(new Employee
                {
                    Id = LeerFilas.GetInt32(0),
                    Office = LeerFilas.GetString(1),
                    EmployeeCode = LeerFilas.GetString(2),
                    EmployeeName = LeerFilas.GetString(3),
                    EmployeeSurname = LeerFilas.GetString(4),
                    Division = LeerFilas.GetString(5),
                    Position = LeerFilas.GetString(6),
                    Grade = LeerFilas.GetInt32(7),
                    BeginDate = LeerFilas.GetDateTime(8),
                    Birthday = LeerFilas.GetDateTime(9),
                    IdentificationNumber = LeerFilas.GetString(10),
                    BaseSalary = LeerFilas.GetDecimal(11),
                    ProductionBonus = LeerFilas.GetDecimal(12),
                    CompensationBonus = LeerFilas.GetDecimal(13),
                    Commission = LeerFilas.GetDecimal(14),
                    Contributions = LeerFilas.GetDecimal(15)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }
       
        //codigos para registrar empleados en la base de datos
        public bool AddEmployee(Employee employee)
        {
            bool respuesta = false;

            try
            {
                
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conexion;
                Conexion.Open();
                cmd.CommandText = "insert into Employees values (@Office,@EmployeeCode,@EmployeeName,@EmployeeSurname,@Division,@Position,@Grade,@BeginDate,@Birthday,@IdentificationNumber,@BaseSalary,@ProductionBonus,@CompensationBonus,@Commission,@Contributions)";
                cmd.Parameters.AddWithValue("@Office", employee.Office);
                cmd.Parameters.AddWithValue("@EmployeeCode", employee.EmployeeCode);
                cmd.Parameters.AddWithValue("@EmployeeName", employee.EmployeeName);
                cmd.Parameters.AddWithValue("@EmployeeSurname", employee.EmployeeSurname);
                cmd.Parameters.AddWithValue("@Division", employee.Division);
                cmd.Parameters.AddWithValue("@Position", employee.Position);
                cmd.Parameters.AddWithValue("@Grade", employee.Grade);
                cmd.Parameters.AddWithValue("@BeginDate", employee.BeginDate);
                cmd.Parameters.AddWithValue("@Birthday", employee.Birthday);
                cmd.Parameters.AddWithValue("@IdentificationNumber", employee.IdentificationNumber);
                cmd.Parameters.AddWithValue("@BaseSalary", employee.BaseSalary);
                cmd.Parameters.AddWithValue("@ProductionBonus", employee.ProductionBonus);
                cmd.Parameters.AddWithValue("@CompensationBonus", employee.CompensationBonus);
                cmd.Parameters.AddWithValue("@Commission", employee.Commission);
                cmd.Parameters.AddWithValue("@Contributions", employee.Contributions);
                cmd.ExecuteNonQuery();
                Conexion.Close();
                respuesta = true;



            }
            catch (Exception)
            {
                respuesta = false;
            }


            return respuesta;
        }

        //codigos para editar los datos empleados
        public bool EditEmployee(Employee employee)
        {
            bool respuesta = false;
            
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conexion;
                Conexion.Open();
                cmd.CommandText = "update Employees set Office=@Office,EmployeeCode=@EmployeeCode,EmployeeName=@EmployeeName,EmployeeSurname=@EmployeeSurname,Division=@Division,Position=@Position,Grade=@Grade,BeginDate=@BeginDate,Birthday=@Birthday,IdentificationNumber=@IdentificationNumber,BaseSalary=@BaseSalary,ProductionBonus=@ProductionBonus,CompensationBonus=@CompensationBonus,Commission=@Commission,Contributions=@Contributions where Id= @Id";
                cmd.Parameters.AddWithValue("@Id", employee.Id);
                cmd.Parameters.AddWithValue("@Office", employee.Office);
                cmd.Parameters.AddWithValue("@EmployeeCode", employee.EmployeeCode);
                cmd.Parameters.AddWithValue("@EmployeeName", employee.EmployeeName);
                cmd.Parameters.AddWithValue("@EmployeeSurname", employee.EmployeeSurname);
                cmd.Parameters.AddWithValue("@Division", employee.Division);
                cmd.Parameters.AddWithValue("@Position", employee.Position);
                cmd.Parameters.AddWithValue("@Grade", employee.Grade);
                cmd.Parameters.AddWithValue("@BeginDate", employee.BeginDate);
                cmd.Parameters.AddWithValue("@Birthday", employee.Birthday);
                cmd.Parameters.AddWithValue("@IdentificationNumber", employee.IdentificationNumber);
                cmd.Parameters.AddWithValue("@BaseSalary", employee.BaseSalary);
                cmd.Parameters.AddWithValue("@ProductionBonus", employee.ProductionBonus);
                cmd.Parameters.AddWithValue("@CompensationBonus", employee.CompensationBonus);
                cmd.Parameters.AddWithValue("@Commission", employee.Commission);
                cmd.Parameters.AddWithValue("@Contributions", employee.Contributions);
                cmd.ExecuteNonQuery();
                Conexion.Close();
                respuesta = true;
            }
            catch (Exception)
            {
                respuesta = false;
            }


            return respuesta;
        }

        //codigo para eliminar un empleado por id
        public bool DeleteEmployee(Employee employee)
        {
            bool respuesta = false;

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Conexion;
                Conexion.Open();
                cmd.CommandText = "Delete from Employees where Id= @Id";
                cmd.Parameters.AddWithValue("@Id", employee.Id);
                cmd.ExecuteNonQuery();
                Conexion.Close();
                respuesta = true;
            }
            catch (Exception)
            {
                respuesta = false;
            }


            return respuesta;
        }

        //Consulta para validar la existencia de un empleado por el IdentificationNumber
        public string ValidarExistenciaEmployee(string IdentificationNumber)
        {
            string Respuesta = "Ninguno";
            try
            {
                
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = Conexion;
                cmd.CommandText = "Select * from Employees where IdentificationNumber = @IdentificationNumber";
                cmd.Parameters.AddWithValue("@IdentificationNumber", IdentificationNumber);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    Respuesta = "Este empleado ya existe en el sistema";
                    Conexion.Close();
                }
                else
                {
                    Respuesta = "Ninguno";
                    Conexion.Close();
                }

                return Respuesta;

            }
            catch (Exception)
            {
                Respuesta = "Error al procesar solicitud de validacion";
                return Respuesta;
            }


        }

        //Codigos para buscar empleados con el mismo office y grado
        public IEnumerable<Employee> GetEmployeeListByOfficeAndGrade(string Office, int Grade)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Id,EmployeeName,EmployeeSurname,Division,Position,BeginDate,Birthday,IdentificationNumber,BaseSalary,ProductionBonus,CompensationBonus,Commission,Contributions from Employees where Office = @Office and Grade = @Grade";
            cmd.Parameters.AddWithValue("@Office", Office);
            cmd.Parameters.AddWithValue("@Grade", Grade);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Employee> ListaGenerica = new List<Employee>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Employee
                {
                    Id = LeerFilas.GetInt32(0),
                    EmployeeName = LeerFilas.GetString(1),
                    EmployeeSurname = LeerFilas.GetString(2),
                    Division = LeerFilas.GetString(3),
                    Position = LeerFilas.GetString(4),
                    BeginDate = LeerFilas.GetDateTime(5),
                    Birthday = LeerFilas.GetDateTime(6),
                    IdentificationNumber = LeerFilas.GetString(7),
                    BaseSalary = LeerFilas.GetDecimal(8),
                    ProductionBonus = LeerFilas.GetDecimal(9),
                    CompensationBonus = LeerFilas.GetDecimal(10),
                    Commission = LeerFilas.GetDecimal(11),
                    Contributions = LeerFilas.GetDecimal(12)

                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //Codigos para buscar empleados con el mismo grado
        public IEnumerable<Employee> GetEmployeeListByGrade(int Grade)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Id,EmployeeName,EmployeeSurname,Division,Position,BeginDate,Birthday,IdentificationNumber,BaseSalary,ProductionBonus,CompensationBonus,Commission,Contributions from Employees where Grade = @Grade";
            cmd.Parameters.AddWithValue("@Grade", Grade);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Employee> ListaGenerica = new List<Employee>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Employee
                {
                    Id = LeerFilas.GetInt32(0),
                    EmployeeName = LeerFilas.GetString(1),
                    EmployeeSurname = LeerFilas.GetString(2),
                    Division = LeerFilas.GetString(3),
                    Position = LeerFilas.GetString(4),
                    BeginDate = LeerFilas.GetDateTime(5),
                    Birthday = LeerFilas.GetDateTime(6),
                    IdentificationNumber = LeerFilas.GetString(7),
                    BaseSalary = LeerFilas.GetDecimal(8),
                    ProductionBonus = LeerFilas.GetDecimal(9),
                    CompensationBonus = LeerFilas.GetDecimal(10),
                    Commission = LeerFilas.GetDecimal(11),
                    Contributions = LeerFilas.GetDecimal(12)

                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //Codigos para buscar empleados con el misma position y grado
        public IEnumerable<Employee> GetEmployeeListByPositionAndGrade(string Position, int Grade)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Id,EmployeeName,EmployeeSurname,Division,Position,BeginDate,Birthday,IdentificationNumber,BaseSalary,ProductionBonus,CompensationBonus,Commission,Contributions from Employees where Position = @Position and Grade = @Grade";
            cmd.Parameters.AddWithValue("@Position", Position);
            cmd.Parameters.AddWithValue("@Grade", Grade);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Employee> ListaGenerica = new List<Employee>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Employee
                {
                    Id = LeerFilas.GetInt32(0),
                    EmployeeName = LeerFilas.GetString(1),
                    EmployeeSurname = LeerFilas.GetString(2),
                    Division = LeerFilas.GetString(3),
                    Position = LeerFilas.GetString(4),
                    BeginDate = LeerFilas.GetDateTime(5),
                    Birthday = LeerFilas.GetDateTime(6),
                    IdentificationNumber = LeerFilas.GetString(7),
                    BaseSalary = LeerFilas.GetDecimal(8),
                    ProductionBonus = LeerFilas.GetDecimal(9),
                    CompensationBonus = LeerFilas.GetDecimal(10),
                    Commission = LeerFilas.GetDecimal(11),
                    Contributions = LeerFilas.GetDecimal(12)

                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

    }
}

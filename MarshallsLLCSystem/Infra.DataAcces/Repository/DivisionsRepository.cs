﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using System.Data.SqlClient;

namespace Infra.DataAcces.Repository
{
    public class DivisionsRepository : ConnectionSQL, IDivisionsRepository
    {
        //Codigos para extraer la lista de Divisions de la base de datos
        public IEnumerable<Divisions> DivisionsList()
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select * from Divisions";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Divisions> ListaGenerica = new List<Divisions>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Divisions
                {
                    Id = LeerFilas.GetInt32(0),
                    NameDivisions = LeerFilas.GetString(1),
                    InformationDivisions = LeerFilas.GetString(2)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Model.Abstractions;
using Dominio.Model.Entities;
using System.Data.SqlClient;

namespace Infra.DataAcces.Repository
{
    //Codigos para extraer la lista de Offices de la base de datos
    public class OficcesRepository : ConnectionSQL, IOficcesRepository
    {
        public IEnumerable<Offices> OfficesList()
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select * from Offices";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<Offices> ListaGenerica = new List<Offices>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new Offices
                {
                    Id = LeerFilas.GetInt32(0),
                    NameOffices = LeerFilas.GetString(1),
                    InformationOffices = LeerFilas.GetString(2)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }
    }
}

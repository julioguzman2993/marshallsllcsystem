﻿using Dominio.Model.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Model.Entities;
using System.Data.SqlClient;
using System.Data;

namespace Infra.DataAcces.Repository
{
    public class SalaryRepository : ConnectionSQL, ISalaryRepository
    {
        //codigo para registrar salario en la base de datos
        public bool AddSalary(Salary salary)
        {
            bool respuesta = false;
            
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            Conexion.Open();
            cmd.CommandText = "insert into Salary values (@IdEmployee,@IdentificationNumber,@BaseSalary,@ProductionBonus,@CompensationBonus,@Commission,@Contributions,@TotalSalary,@PayYear,@PayMonth,@PayDay)";
            cmd.Parameters.AddWithValue("@IdEmployee", salary.IdEmployee);
            cmd.Parameters.AddWithValue("@IdentificationNumber", salary.IdentificationNumber);
            cmd.Parameters.AddWithValue("@BaseSalary", salary.BaseSalary);
            cmd.Parameters.AddWithValue("@ProductionBonus", salary.ProductionBonus);
            cmd.Parameters.AddWithValue("@CompensationBonus", salary.CompensationBonus);
            cmd.Parameters.AddWithValue("@Commission", salary.Commission);
            cmd.Parameters.AddWithValue("@Contributions", salary.Contributions);
            cmd.Parameters.AddWithValue("@TotalSalary", salary.TotalSalary);
            cmd.Parameters.AddWithValue("@PayYear", salary.PayYear);
            cmd.Parameters.AddWithValue("@PayMonth", salary.PayMonth);
            cmd.Parameters.AddWithValue("@PayDay", salary.PayDay);
            cmd.ExecuteNonQuery();
            Conexion.Close();
            respuesta = true;
            try
            {
                
            }
            catch (Exception)
            {
                respuesta = false;
            }
            

            return respuesta;
        }
        
        //codigos para validar que el empledo no tengo el year y month pagado 
        public string ValidarPagoMes(List<Salary> ListaPagoSalario)
        {

            string MesPago = "Ninguno";
            try
            {
                int index = 0;

                SqlCommand cmd = new SqlCommand();
                foreach (var data in ListaPagoSalario)
                {
                    index = index + 1;

                    cmd.Connection = Conexion;
                    cmd.CommandText = "Select * from Salary where IdEmployee = @IdEmployee" + index.ToString() + " and PayYear = @PayYear" + index.ToString() + " and PayMonth = @PayMonth" + index.ToString();
                    cmd.Parameters.AddWithValue("@IdEmployee" + index.ToString(), Convert.ToInt32(data.IdEmployee));
                    cmd.Parameters.AddWithValue("@PayYear" + index.ToString(), Convert.ToInt32(data.PayYear));
                    cmd.Parameters.AddWithValue("@PayMonth" + index.ToString(), Convert.ToInt32(data.PayMonth));
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        MesPago = "Este empleado tiene registrado como pago el mes " + data.PayMonth.ToString() + " del " + data.PayYear.ToString() + " (Seleccione otre mes para el Pago Salarial)";
                        
                        Conexion.Close();
                        //return MesPago;
                    }
                    else
                    {
                        MesPago = "Ninguno";
                        Conexion.Close();
                    }


                }
                Conexion.Close();

                return MesPago;

            }
            catch (Exception)
            {
                MesPago = "Error al procesar solicitud de validacion";
                return MesPago;
            }
            

        }

        //obtener la lista de salarios por year y month
        public IEnumerable<ConsultaSalaryModel> GetSalaryYearAndMonth(int PayYear,int PayMonth)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Employees.Id,Employees.IdentificationNumber,Salary.TotalSalary,Salary.PayYear,Salary.PayMonth,Office,EmployeeName,EmployeeSurname,Division,Position,Grade,BeginDate,Birthday from Employees inner join Salary on Salary.IdEmployee = Employees.Id where Salary.PayYear = @PayYear and Salary.PayMonth = @PayMonth order by Salary.Id desc";
            cmd.Parameters.AddWithValue("@PayYear", PayYear);
            cmd.Parameters.AddWithValue("@PayMonth", PayMonth);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    Id = LeerFilas.GetInt32(0),
                    IdentificationNumber = LeerFilas.GetString(1),
                    TotalSalary = LeerFilas.GetDecimal(2),
                    PayYear = LeerFilas.GetInt32(3),
                    PayMonth = LeerFilas.GetInt32(4),
                    Office = LeerFilas.GetString(5),
                    EmployeeName = LeerFilas.GetString(6),
                    EmployeeSurname = LeerFilas.GetString(7),
                    Division = LeerFilas.GetString(8),
                    Position = LeerFilas.GetString(9),
                    Grade = LeerFilas.GetInt32(10),
                    BeginDate = LeerFilas.GetDateTime(11),
                    Birthday = LeerFilas.GetDateTime(12)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //obtener la lista de salarios por id de empleado
        public IEnumerable<ConsultaSalaryModel> GetSalaryByIdEmployee(int IdEmployee)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Employees.Id,Employees.IdentificationNumber,Salary.TotalSalary,Salary.PayYear,Salary.PayMonth,Office,EmployeeName,EmployeeSurname,Division,Position,Grade,BeginDate,Birthday from Employees inner join Salary on Salary.IdEmployee = Employees.Id where Employees.Id = @IdEmployee order by Salary.Id desc";
            cmd.Parameters.AddWithValue("@IdEmployee", IdEmployee);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    Id = LeerFilas.GetInt32(0),
                    IdentificationNumber = LeerFilas.GetString(1),
                    TotalSalary = LeerFilas.GetDecimal(2),
                    PayYear = LeerFilas.GetInt32(3),
                    PayMonth = LeerFilas.GetInt32(4),
                    Office = LeerFilas.GetString(5),
                    EmployeeName = LeerFilas.GetString(6),
                    EmployeeSurname = LeerFilas.GetString(7),
                    Division = LeerFilas.GetString(8),
                    Position = LeerFilas.GetString(9),
                    Grade = LeerFilas.GetInt32(10),
                    BeginDate = LeerFilas.GetDateTime(11),
                    Birthday = LeerFilas.GetDateTime(12)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //obtener la lista de salarios por el EmployeeName
        public IEnumerable<ConsultaSalaryModel> GetSalaryByEmployeeName(string EmployeeName)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Employees.Id,Employees.IdentificationNumber,Salary.TotalSalary,Salary.PayYear,Salary.PayMonth,Office,EmployeeName,EmployeeSurname,Division,Position,Grade,BeginDate,Birthday from Employees inner join Salary on Salary.IdEmployee = Employees.Id where EmployeeName like ('"+ EmployeeName + "%') order by Salary.Id desc";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    Id = LeerFilas.GetInt32(0),
                    IdentificationNumber = LeerFilas.GetString(1),
                    TotalSalary = LeerFilas.GetDecimal(2),
                    PayYear = LeerFilas.GetInt32(3),
                    PayMonth = LeerFilas.GetInt32(4),
                    Office = LeerFilas.GetString(5),
                    EmployeeName = LeerFilas.GetString(6),
                    EmployeeSurname = LeerFilas.GetString(7),
                    Division = LeerFilas.GetString(8),
                    Position = LeerFilas.GetString(9),
                    Grade = LeerFilas.GetInt32(10),
                    BeginDate = LeerFilas.GetDateTime(11),
                    Birthday = LeerFilas.GetDateTime(12)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //obtener la lista de salarios por el EmployeeSurname
        public IEnumerable<ConsultaSalaryModel> GetSalaryByEmployeeSurname(string EmployeeSurname)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Employees.Id,Employees.IdentificationNumber,Salary.TotalSalary,Salary.PayYear,Salary.PayMonth,Office,EmployeeName,EmployeeSurname,Division,Position,Grade,BeginDate,Birthday from Employees inner join Salary on Salary.IdEmployee = Employees.Id where EmployeeSurname like ('" + EmployeeSurname + "%') order by Salary.Id desc";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    Id = LeerFilas.GetInt32(0),
                    IdentificationNumber = LeerFilas.GetString(1),
                    TotalSalary = LeerFilas.GetDecimal(2),
                    PayYear = LeerFilas.GetInt32(3),
                    PayMonth = LeerFilas.GetInt32(4),
                    Office = LeerFilas.GetString(5),
                    EmployeeName = LeerFilas.GetString(6),
                    EmployeeSurname = LeerFilas.GetString(7),
                    Division = LeerFilas.GetString(8),
                    Position = LeerFilas.GetString(9),
                    Grade = LeerFilas.GetInt32(10),
                    BeginDate = LeerFilas.GetDateTime(11),
                    Birthday = LeerFilas.GetDateTime(12)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //obtener la lista de salarios por IdentificationNumber
        public IEnumerable<ConsultaSalaryModel> GetSalaryByIdentificationNumber(string IdentificationNumber)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select Employees.Id,Employees.IdentificationNumber,Salary.TotalSalary,Salary.PayYear,Salary.PayMonth,Office,EmployeeName,EmployeeSurname,Division,Position,Grade,BeginDate,Birthday from Employees inner join Salary on Salary.IdEmployee = Employees.Id where Employees.IdentificationNumber like ('" + IdentificationNumber + "%') order by Salary.Id desc";
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    Id = LeerFilas.GetInt32(0),
                    IdentificationNumber = LeerFilas.GetString(1),
                    TotalSalary = LeerFilas.GetDecimal(2),
                    PayYear = LeerFilas.GetInt32(3),
                    PayMonth = LeerFilas.GetInt32(4),
                    Office = LeerFilas.GetString(5),
                    EmployeeName = LeerFilas.GetString(6),
                    EmployeeSurname = LeerFilas.GetString(7),
                    Division = LeerFilas.GetString(8),
                    Position = LeerFilas.GetString(9),
                    Grade = LeerFilas.GetInt32(10),
                    BeginDate = LeerFilas.GetDateTime(11),
                    Birthday = LeerFilas.GetDateTime(12)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;
        }

        //obtener la lista de salarios para los tres meses consecutivos
        public IEnumerable<ConsultaSalaryModel> GetSalaryByTresMesesConsecutivos(int IdEmployee)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select top 3 PayMonth,Salary.Id,Employees.IdentificationNumber,Employees.EmployeeName,Employees.EmployeeSurname,Salary.TotalSalary,PayYear from Salary inner join Employees on Employees.Id = Salary.IdEmployee where IdEmployee=@IdEmployee and PayYear = (select top 1 PayYear from salary order by PayYear desc) order by PayMonth desc";
            cmd.Parameters.AddWithValue("@IdEmployee", IdEmployee);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    PayMonth = LeerFilas.GetInt32(0),
                    Id = LeerFilas.GetInt32(1),
                    IdentificationNumber = LeerFilas.GetString(2),
                    EmployeeName = LeerFilas.GetString(3),
                    EmployeeSurname = LeerFilas.GetString(4),
                    TotalSalary = LeerFilas.GetDecimal(5),
                    PayYear = LeerFilas.GetInt32(6)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;

        }

        //obtener la lista de salarios para los dos meses consecutivos
        public IEnumerable<ConsultaSalaryModel> GetSalaryByDosMesesConsecutivos(int IdEmployee)
        {
            SqlDataReader LeerFilas;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Conexion;
            cmd.CommandText = "select top 2 PayMonth,Salary.Id,Employees.IdentificationNumber,Employees.EmployeeName,Employees.EmployeeSurname,Salary.TotalSalary,PayYear from Salary inner join Employees on Employees.Id = Salary.IdEmployee where IdEmployee=@IdEmployee and PayYear = (select top 1 PayYear from salary order by PayYear desc) order by PayMonth desc";
            cmd.Parameters.AddWithValue("@IdEmployee", IdEmployee);
            Conexion.Open();
            LeerFilas = cmd.ExecuteReader();
            List<ConsultaSalaryModel> ListaGenerica = new List<ConsultaSalaryModel>();
            while (LeerFilas.Read())
            {
                ListaGenerica.Add(new ConsultaSalaryModel
                {
                    PayMonth = LeerFilas.GetInt32(0),
                    Id = LeerFilas.GetInt32(1),
                    IdentificationNumber = LeerFilas.GetString(2),
                    EmployeeName = LeerFilas.GetString(3),
                    EmployeeSurname = LeerFilas.GetString(4),
                    TotalSalary = LeerFilas.GetDecimal(5),
                    PayYear = LeerFilas.GetInt32(6)
                });

            }
            LeerFilas.Close();
            Conexion.Close();
            return ListaGenerica;

        }

    }
}

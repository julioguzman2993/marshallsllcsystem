﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Entities
{
    public class Salary
    {
        public int Id { get; set; }
        public int IdEmployee { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
        public decimal TotalSalary { get; set; }
        public int PayYear { get; set; }
        public int PayMonth { get; set; }
        public int PayDay { get; set; }
    }
}

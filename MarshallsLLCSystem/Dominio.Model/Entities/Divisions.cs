﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Entities
{
    public class Divisions
    {
        public int Id { get; set; }
        public string NameDivisions { get; set; }
        public string InformationDivisions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Entities
{
    public class Positions
    {
        public int Id { get; set; }
        public string NamePositions { get; set; }
        public string InformationPositions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Entities
{
    public class Offices
    {
        public int Id { get; set; }
        public string NameOffices { get; set; }
        public string InformationOffices { get; set; }
    }
}

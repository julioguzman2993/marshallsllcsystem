﻿using Dominio.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Validaciones
{
    public class ValidarCamposEmployee
    {
        //Codigos para validar que los campos no entren vacio al repositorio de datos en la capa de infraestructura
        public bool ValidarDatosEmployee(Employee employee)
        {
            bool respuesta = false;
            if (employee.Office == "" || employee.EmployeeCode == "" || employee.EmployeeName == "" || employee.EmployeeSurname == "" || employee.Division == "")
            {
                respuesta = false;
            }

            if (employee.Position == "" || employee.Grade.ToString() == "" || employee.BeginDate.ToString() == "" || employee.Birthday.ToString() == "" || employee.IdentificationNumber == "")
            {
                respuesta = false;
            }

            if (employee.BaseSalary.ToString() == "" || employee.ProductionBonus.ToString() == "" || employee.CompensationBonus.ToString() == "" || employee.Commission.ToString() == "" || employee.Contributions.ToString() == "")
            {
                respuesta = false;
            }

            else
            {
                respuesta = true;
            }

            return respuesta;

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.ObjectValues
{
    //clase para obtener la fecha actual
    public class GetActualDate
    {
        DateTime FechaActual = DateTime.Now;

        //Clase para obtener el dia actual
        public int GetActualDay()
        {
            
            int fechaActual = FechaActual.Day;

            return fechaActual;
        }

        //Clase para obtener el mes actual
        public int GetActualMonth()
        {

            int fechaActual = FechaActual.Month;

            return fechaActual;
        }

        //Clase para obtener el year actual
        public int GetActualYear()
        {

            int fechaActual = FechaActual.Year;

            return fechaActual;
        }

        //Clase para cargar todos los years determinados desde el actual year en el DropDownListFor de la vista
        public IEnumerable<classListYears> GetListYear()
        {
            List<classListYears> objClassListYears = new List<classListYears>();
            int actualYear = FechaActual.Year;
            for (int i = (actualYear - 5); i <= 2100; i++)
            {
                
                objClassListYears.Add(new classListYears {ListYears = i });
            }
            
            return objClassListYears;
        }

        public class classListYears
        {
            public int ListYears { get; set; }

        }

        //Clase para cargar todos los meses determinados en el DropDownListFor de la vista
        public IEnumerable<classListMonth> GetListMonth()
        {
            List<classListMonth> objClassListMonths = new List<classListMonth>();
            int actualYear = FechaActual.Year;
            for (int i = 1; i <= 12; i++)
            {

                objClassListMonths.Add(new classListMonth { ListMonths = i });
            }

            return objClassListMonths;
        }

        public class classListMonth
        {
            public int ListMonths { get; set; }

        }

    }
}

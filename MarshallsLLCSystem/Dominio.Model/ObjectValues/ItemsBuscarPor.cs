﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.ObjectValues
{
    public class ItemsBuscarPor
    {
        //Codigos para cargar los DropDownListFor de las vistas para realizar el filtro hacia el acceso a datos.
        public IEnumerable<ItemsBusc> ListaItems()
        {
            List<ItemsBusc> listItemsBusc = new List<ItemsBusc>(){
                new ItemsBusc() { BuscarPor = "Todos"},
               new ItemsBusc() { BuscarPor = "IdEmployee"},
               new ItemsBusc() { BuscarPor = "EmployeeName"},
               new ItemsBusc() { BuscarPor = "EmployeeSurname"},
               new ItemsBusc() { BuscarPor = "IdentificationNumber"},
             };

            return listItemsBusc;
        }

        public IEnumerable<ItemsBuscEmpleado> ListaItemsBuscEmpleado()
        {
            List<ItemsBuscEmpleado> listItems = new List<ItemsBuscEmpleado>(){
                new ItemsBuscEmpleado() { BuscarPor = "Misma Oficina y Grado"},
               new ItemsBuscEmpleado() { BuscarPor = "Todas Oficinas y Mismo Grado"},
               new ItemsBuscEmpleado() { BuscarPor = "Misma Pocision y Grado"},
               new ItemsBuscEmpleado() { BuscarPor = "Tadas Pocisiones y Mismo Grado"}
             };

            return listItems;
        }

        public IEnumerable<ItemsBuscSalario> ListaItemsBuscSalario()
        {
            List<ItemsBuscSalario> listItems = new List<ItemsBuscSalario>(){
               new ItemsBuscSalario() { BuscarPor = "2 meses consecutivos"},
               new ItemsBuscSalario() { BuscarPor = "3 meses consecutivos"}
             };

            return listItems;
        }

        public class ItemsBusc
        {
            public string BuscarPor { get; set; }
            
        }

        public class ItemsBuscEmpleado
        {
            public string BuscarPor { get; set; }

        }

        public class ItemsBuscSalario
        {
            public string BuscarPor { get; set; }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.ObjectValues
{
    //Clase para calcular el salario
    public class CalculoSalario
    {
        public decimal CalcluloTotalSalary(decimal BaseSalary, decimal ProductionBonus, decimal CompensationBonus, decimal Commission, decimal Contributions)
        {
            decimal SalarioTotal = 0;
            decimal OtherIncome = 0;

            OtherIncome = (BaseSalary + Commission) * Convert.ToDecimal(0.08) + Commission;

            SalarioTotal = BaseSalary + ProductionBonus + (CompensationBonus * Convert.ToDecimal(0.75)) + OtherIncome - Contributions;

            return SalarioTotal;
        }

    }
}

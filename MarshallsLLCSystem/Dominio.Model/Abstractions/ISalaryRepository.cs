﻿using Dominio.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Abstractions
{
    //interfaces para acceder a los metodos de Salarios previamente estructurados
    public interface ISalaryRepository
    {
        bool AddSalary(Salary salary);

        string ValidarPagoMes(List<Salary> ListaPagoSalario);

        IEnumerable<ConsultaSalaryModel> GetSalaryYearAndMonth(int PayYear, int PayMonth);

        IEnumerable<ConsultaSalaryModel> GetSalaryByIdEmployee(int IdEmployee);

        IEnumerable<ConsultaSalaryModel> GetSalaryByEmployeeName(string EmployeeName);

        IEnumerable<ConsultaSalaryModel> GetSalaryByEmployeeSurname(string EmployeeSurname);

        IEnumerable<ConsultaSalaryModel> GetSalaryByIdentificationNumber(string IdentificationNumber);

        IEnumerable<ConsultaSalaryModel> GetSalaryByTresMesesConsecutivos(int IdEmployee);

        IEnumerable<ConsultaSalaryModel> GetSalaryByDosMesesConsecutivos(int IdEmployee);

    }
}

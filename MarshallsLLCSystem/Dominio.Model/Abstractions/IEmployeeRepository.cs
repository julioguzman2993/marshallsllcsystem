﻿using Dominio.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Abstractions
{
    //interfaces para acceder a los metodos de Empleados previamente estructurados
    public interface IEmployeeRepository
    {
        
        IEnumerable<Employee> GetEmployeeAll();

        IEnumerable<Employee> BuscEmpID(int id);

        bool AddEmployee(Employee employee);

        bool EditEmployee(Employee employee);

        bool DeleteEmployee(Employee employee);

        string ValidarExistenciaEmployee(string IdentificationNumber);
        IEnumerable<Employee> GetEmployeeListByOfficeAndGrade(string Office, int Grade);
        IEnumerable<Employee> GetEmployeeListByGrade(int Grade);
        IEnumerable<Employee> GetEmployeeListByPositionAndGrade(string Position, int Grade);


    }
}

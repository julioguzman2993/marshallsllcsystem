﻿using Dominio.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Model.Abstractions
{
    //interface para acceder a los metodos de Offices previamente estructurados
    public interface IOficcesRepository
    {
        IEnumerable<Offices> OfficesList();
    }
}

USE [MarshallsLLC]
GO
/****** Object:  Table [dbo].[Divisions]    Script Date: 11/05/2021 0:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Divisions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameDivisions] [varchar](50) NOT NULL,
	[InformationDivisions] [varchar](100) NULL,
 CONSTRAINT [PK_Divisions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 11/05/2021 0:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Office] [varchar](50) NULL,
	[EmployeeCode] [varchar](10) NULL,
	[EmployeeName] [varchar](150) NULL,
	[EmployeeSurname] [varchar](150) NULL,
	[Division] [varchar](50) NULL,
	[Position] [varchar](50) NULL,
	[Grade] [int] NULL,
	[BeginDate] [date] NULL,
	[Birthday] [date] NULL,
	[IdentificationNumber] [varchar](10) NULL,
	[BaseSalary] [decimal](18, 2) NULL,
	[ProductionBonus] [decimal](18, 2) NULL,
	[CompensationBonus] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Contributions] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Offices]    Script Date: 11/05/2021 0:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Offices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameOffices] [varchar](50) NOT NULL,
	[InformationOffices] [varchar](100) NULL,
 CONSTRAINT [PK_Offices_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 11/05/2021 0:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Positions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NamePositions] [varchar](50) NOT NULL,
	[InformationPositions] [varchar](100) NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Salary]    Script Date: 11/05/2021 0:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEmployee] [int] NULL,
	[IdentificationNumber] [varchar](10) NULL,
	[BaseSalary] [decimal](18, 2) NULL,
	[ProductionBonus] [decimal](18, 2) NULL,
	[CompensationBonus] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Contributions] [decimal](18, 2) NULL,
	[TotalSalary] [decimal](18, 2) NULL,
	[PayYear] [int] NULL,
	[PayMonth] [int] NULL,
	[PayDay] [int] NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
